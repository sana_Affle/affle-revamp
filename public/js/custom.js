$(document).ready(function(){
    //  Menu script code
    $('.breadcrumb-box').click(function(){
        $('body').addClass('menu-show');
        $('body').removeClass('investor-menu-show');
    });
    $('.close-icon a').click(function(){
        $('body').removeClass('menu-show');
        $('body').removeClass('investor-menu-show');
        $('.has-child').removeClass('child-menu-show');
    });
    $('.investor-menu').click(function(){
        $('body').addClass('investor-menu-show');
        $('body').removeClass('menu-show');
    });
    $('.has-child a').click(function(){
        $(this).parent().toggleClass('child-menu-show').siblings().removeClass('child-menu-show');
    });
    // 

    $('.show-full').click(function(){
        $('.product-milestone').addClass('show');
        $(this).hide();
        $('.show-less').show();
    });

    $('.show-less').click(function(){
        $('.product-milestone').removeClass('show');
        $(this).hide();
        $('.show-full').show();
    });
    
});

$(document).on('click', 'a[href^="#top"]', function () {
    e.preventDefault();
    $('html, body').stop().animate({
      scrollTop: $($(this).attr('href')).offset().top
    }, 1000);
});

$(document).ready(function() {
    $('.popup-box').click(function(){
        $('body').addClass('popup-triggered');
    });

    $('.close-popup').click(function(){
        $('body').removeClass('popup-triggered');
        $('iframe').attr('src', $('iframe').attr('src'));
    });
});

$(".form-input").focus(function() {
    let parent = $(this).parent();
    if (!parent.hasClass("active")) {
        parent.addClass("active")
    }
});
  
// remove an "active" class on blur if no content
$(".form-input").blur(function() {
    let self = $(this);
    let parent = self.parent();
    if (parent.hasClass("active") && self.val().length === 0) {
        parent.removeClass("active")
    }
});

$(document).ready(function() {
    $(".members-list-item .modal").modal({
        show: false,
        backdrop: 'static'
    });
});