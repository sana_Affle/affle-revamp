<section class="ad-campaigns-section">
    <div class="ad-campaigns-section-inner">
        <div class="content-box">
            <h3 class="title">Want to Power-Up Your Advertising Campaigns With Affle's Solutions and Platforms?</h3>
            <a href="#" class="btn btn--primary">Get In Touch</a>
        </div>
        <div class="image-box">
            <div class="image">
                <img src="{{asset('images/ad-campaign.jpg')}}" alt="" class="img-fluid" loading="lazy">
            </div>
        </div>
    </div>
</section>

<section class="newsletter-section">
    <div class="newsletter-section-inner">
        <div class="col-xl-6 col-md-10 newsltter-content">
            <h3 class="title">Subscribe to our Newsletter</h3>
            <p>To find out more about us and our platforms and business</p>
            <form method="post" class="newsletter-form">
                <input type="email" name="email" placeholder="Email address" class="form-control">
                <input type="submit" name="submit" value="Get Started" class="btn btn--primary">
            </form>
        </div>
    </div>
</section>