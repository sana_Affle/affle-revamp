<header class="header-section <?php if(Request::is('/','discover-identify')){echo 'home-header';} ?>">
    <div class="header-section-inner">
        <div class="logo-box">
            <a href="#">
                <img src="{{asset('images/logo.png')}}" alt="Affle Logo" class="img-fluid">
            </a>
        </div>
        <div class="navigation">
            <ul class="menu">
                <li class="menu-item active">
                    <a href="#">Discover & Identify</a>
                </li>
                <li class="menu-item">
                    <a href="#">Acquire & Re-engage</a>
                </li>
                <li class="menu-item">
                    <a href="#">Re-Engage & Transact</a>
                </li>
            </ul>
        </div>
        <div class="breadcrumb-box">
            <a href="#">
                <span>Menu</span>
                <img src="{{asset('images/menu.svg')}}" alt="" class="img-fluid">
            </a>
        </div>
    </div>
</header>
    
<div class="secondary-menu">
    <div class="secondary-menu-inner">
        <div class="close-icon">
            <a href="#">
                <img src="{{asset('images/close-icon.svg')}}" alt="" class="img-fluid">
            </a>
        </div>
        <ul class="menu">
            <li class="menu-item">
                <a href="#">About Us</a>
            </li>
            <li class="menu-item">
                <a href="#">Discover & Identify</a>
            </li>
            <li class="menu-item">
                <a href="#">Acquire & Re-engage</a>
            </li>
            <li class="menu-item">
                <a href="#">Re-Engage & Transact</a>
            </li>
            <li class="menu-item">
                <a href="#">Digital Transformation</a>
            </li>
            <li class="menu-item">
                <a href="#">Investor Relations</a>
            </li>
            <li class="menu-item">
                <a href="#">Careers</a>
            </li>
            <li class="menu-item">
                <a href="#">Media</a>
            </li>
            <li class="menu-item">
                <a href="#">Contact Us</a>
            </li>
        </ul>
        <div class="social-list">
            <a href="#" class="social-item">
                <img src="{{asset('images/facebook.svg')}}" alt="" class="img-fluid">
            </a>
            <a href="#" class="social-item">
                <img src="{{asset('images/twitter.svg')}}" alt="" class="img-fluid">
            </a>
            <a href="#" class="social-item">
                <img src="{{asset('images/instagram.svg')}}" alt="" class="img-fluid">
            </a>
            <a href="#" class="social-item">
                <img src="{{asset('images/linkedin.svg')}}" alt="" class="img-fluid">
            </a>
            <a href="#" class="social-item">
                <img src="{{asset('images/youtube.svg')}}" alt="" class="img-fluid">
            </a>
        </div>
    </div>
</div>