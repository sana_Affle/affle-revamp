<div class="secondary-menu investor-menu-list">
    <div class="secondary-menu-inner">
        <div class="close-icon">
            <a href="#">
                <img src="{{asset('images/close-icon.svg')}}" alt="" class="img-fluid">
            </a>
        </div>
        <ul class="menu">
            <li class="menu-item">
                <a href="#">IR Home</a>
            </li>
            <li class="menu-item has-child">
                <a href="#">Presentations</a>
                <ul class="submenu">
                    <li class="submenu-item">
                        <a href="#">Latest Earnings Presentation</a>
                    </li>
                    <li class="submenu-item">
                        <a href="#">Corporate Presentation</a>
                    </li>
                    <li class="submenu-item">
                        <a href="#">Annual Report</a>
                    </li>
                    <li class="submenu-item">
                        <a href="#">Investor Presentation</a>
                    </li>
                </ul>
            </li>
            <li class="menu-item">
                <a href="#">Financial Details</a>
            </li>
            <li class="menu-item">
                <a href="#">Exchange Announcements</a>
            </li>
            <li class="menu-item">
                <a href="#">Corporate Governance</a>
            </li>
            <li class="menu-item">
                <a href="#">Shareholders & AGM Information</a>
            </li>
            <li class="menu-item">
                <a href="#">Corporate Actions</a>
            </li>
            <li class="menu-item">
                <a href="#">IR Contact</a>
            </li>
            <li class="menu-item has-child">
                <a href="#">Quick Links</a>
                <ul class="submenu">
                    <li class="submenu-item">
                        <a href="#">Option 1</a>
                    </li>
                    <li class="submenu-item">
                        <a href="#">Option 2</a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>