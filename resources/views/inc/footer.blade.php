<footer class="footer-section">
    <div class="footer-section-inner">
        <div class="footer-box footer-box1">
            <div class="footer-box-inner">
         
                <img src="{{asset('images/logo.png')}}" alt="" class="img-fluid">
                <p>Affle is a global technology company with a proprietary consumer intelligence platform that delivers consumer engagement, acquisitions and transactions through relevant Mobile Advertising.</p>
            </div>
        </div>
        <div class="footer-box footer-box2">
            <div class="footer-box-inner">
                <h4 class="title">Solutions</h4>
                <ul class="list">
                    <li class="list-item"><a href="#">Discover & Indentity</a></li>
                    <li class="list-item"><a href="#">Acquire & Engage</a></li>
                    <li class="list-item"><a href="#">Re-Engage & Transact</a></li>
                </ul>
            </div>
        </div>
        <div class="footer-box footer-box3">
            <div class="footer-box-inner">
                <h4 class="title">Quick Links</h4>
                <ul class="list">
                    <li class="list-item"><a href="#">About</a></li>
                    <li class="list-item"><a href="#">Awards</a></li>
                    <li class="list-item"><a href="#">Newsroom</a></li>
                    <li class="list-item"><a href="#">Contact Us</a></li>
                    <li class="list-item"><a href="#">Careers</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="footer-section-bottom">
        <div class="links">
            <a href="#">Terms & Conditions</a>
            <a href="#">Privacy Policy</a>
        </div>
        
        <div class="footer-section-social">
            <a href="#" class="social-item">
                <img src="{{asset('images/facebook.svg')}}" alt="" class="img-fluid">
            </a>
            <a href="#" class="social-item">
                <img src="{{asset('images/twitter.svg')}}" alt="" class="img-fluid">
            </a>
            <a href="#" class="social-item">
                <img src="{{asset('images/instagram.svg')}}" alt="" class="img-fluid">
            </a>
            <a href="#" class="social-item">
                <img src="{{asset('images/linkedin.svg')}}" alt="" class="img-fluid">
            </a>
            <a href="#" class="social-item">
                <img src="{{asset('images/youtube.svg')}}" alt="" class="img-fluid">
            </a>
        </div>

        <div class="copyright">
            <span>&copy; 2021 - Affle. All rights reserved.</span>
        </div>
    </div>
</footer>