  <div class="footer-bottom">
              <div class="container">
                <div class="footer-bottom-inner clearfix">
                  <div class="footer-navigation clearfix">
                  </div>
                  <!-- End .footer-navigation -->
                  <div class="copyright clearfix">
                    <div class="row">
                      <div class="col-md-6 text-left">Copyright @<?php echo date("Y");?> <a href="https://www.affle.com">Affle</a>. All Rights Reserved</div>
                      <div class="col-md-6 text-right">
                        <ul class="footer-social-icons">
                          <li>
                            <a href="https://www.facebook.com/affle" target="_blank" class="fa fa-facebook"></a>
                          </li>
                          <li>
                            <a href="https://twitter.com/affle"target="_blank" class="fa fa-twitter"></a>
                          </li>
                          <li>
                            <a href="https://www.linkedin.com/company/affle/"target="_blank" class="fa fa-linkedin"></a>
                          </li>
                       <!--     <li>
                            <a href="https://dribbble.com/AffleEnterprise"target="_blank" class="fa fa-dribbble"></a>
                          </li>
                          <li>
                              <a href="https://www.behance.net/AffleEnterprise"target="_blank" class="fa fa-behance"></a>
                            </li> -->
                        </ul>
                      </div>
                    </div>
                  </div>
                  <!-- End .copyright -->
                </div>
              </div>
            </div>
            <!-- End .footer-bottom -->
