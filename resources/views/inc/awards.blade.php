<section class="awards-section">
    <div class="awards-section-inner">
        <h3 class="title text-center">Awards & Recognitions</h3>
        <div class="row awards-box-section">
            <div class="col-xl-3 col-sm-6 mb-5 mb-xl-0 awards-box">
                <div class="awards-box-inner">
                    <div class="awards-img">
                        <img src="{{asset('images/indawards.png')}}" alt="" class="img-fluid">
                    </div>
                    <h3 class="title">Indian Digital Awards</h3>
                    <span>7 Awards In Various Catagory</span>
                </div>
            </div>
            <div class="col-xl-3 col-sm-6 mb-5 mb-xl-0 awards-box">
                <div class="awards-box-inner">
                    <div class="awards-img">
                        <img src="{{asset('images/maddies.png')}}" alt="" class="img-fluid">
                    </div>
                    <h3 class="title">The Maddies</h3>
                    <span>9 Awards in various categories</span>
                </div>
            </div>
            <div class="col-xl-3 col-sm-6 awards-box">
                <div class="awards-box-inner">
                    <div class="awards-img">
                        <img src="{{asset('images//mma.png')}}" alt="" class="img-fluid">
                    </div>
                    <h3 class="title">MMA Smarties</h3>
                    <span>9 Awards in various categories</span>
                </div>
            </div>
            <div class="col-xl-3 col-sm-6 awards-box">
                <div class="awards-box-inner">
                    <div class="awards-img">
                        <img src="{{asset('images/image136.png')}}" alt="" class="img-fluid">
                    </div>
                    <h3 class="title">Indian Digital Media Awards</h3>
                    <span>4 Awards in various categories</span>
                </div>
            </div>
        </div>
        <div class="all-awards text-center">
            <a href="#">View All Awards <svg xmlns="http://www.w3.org/2000/svg" width="30" height="15.663" viewBox="0 0 30 15.663"><defs><style>.a{fill:#0255A8;
                ;}</style></defs><path class="a" d="M28.463,44.668l-1.537-1.558-5.2,5.2V22.5H19.537V48.311l-5.2-5.2L12.8,44.668,20.632,52.5Z" transform="translate(-22.5 28.463) rotate(-90)"/></svg></a>
        </div>
        <div class="work-place">
            <img src="{{asset('images/great-place-to-work.png')}}" alt="Great Place To Work" class="img-fluid" loading="lazy">
        </div>
    </div>
</section>