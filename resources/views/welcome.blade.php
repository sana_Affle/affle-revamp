@include('inc.header')
@section('content')
<div class="wrapper" id="home-page">
   <section class="home-main section">
      <div class="home-content">
         <h1 class="hero-title aos-init aos-animate" data-aos="fade-up" data-aos-delay="400" data-aos-once="true">Discover and Convert User Journeys on Connected Devices
         </h1>
         <p class=" sub-title">Build real customer connections throughtout the user's journeys to drive user acquisition, engagement, and conversions. </p>
         <button class="hero-btn"> <a  href="#transform">Learn More <img src="{{asset('/images/icn_down_arrow.svg')}}"  width="17px"
height= "19px"></a></button>
      </div>
   </section>
</div>
<div class="wrapper">
   <section class="home-transform" id="transform">
      <div class="transform-content">
         <h2 class="sec-title ">Transform Ads Into Recommendations With Data-Driven Ad Solutions
         </h2>
         <p class=" sub-title-sec">Discover and identify your most valuable users, and then continue to acquire and re-engage them through Affle's platforms and offerings. Designed to help marketers connect with consumers, our deep-learning, AI-powered algorithms transform ads into powerful consumer recommendations to deliver enhanced ROI and engagement. </p>
         <div class="inner space-40">
            <div class="discover">  
              <h2 class="orange-head">Discover & Identify</h2>
              <ul class="white-head">
                <li>Audience Data Intellienge </li>
                <li>App Discovery</li>
                <li>Ad Fraud Prevention</li>
                <li>Proximity Marketing & Connected TV</li>
              </ul></div>
            <div class="acquire"> 
             <h2 class="orange-head">  Acquire & Re-engage  </h2>
            <ul class="white-head">
                <li>Programmatic Acquisition & Retention</li>
                <li>App Recommendations</li>
                <li>Omnichannel & CRM</li>
                <li>Unified Audience Platform</li>
              </ul></div>
         </div>
      </div>
   </section>
</div>
<div class="wrapper">
   <section class="home-vernacular ">
      <div class="vernacular-content">
         <h2 class="vernacular-title ">Vernacular and Verticalization: Driving Our Growth Story<h2>
         <p class=" sub-title-vernacular">Our growth story is anchored in the 2Vs - vernacular and verticalization. Aiming to reach >10Bn connected devices including connected TV, personal wearables, and smart household appliances, we create unique and integrated consumer journeys for marketers to get more out of their advertising. </p>
         
      </div>
      <div class="inner-ver space-40">
        <div class="col-2"><img src="{{asset('/images/vernacular_image.png')}}"></div>
        <div class="col-2 right-text"><h2>Vernacular</h2><br>
        <p>Create hyper-personalized actionable advertising that speaks to consumers in their own languages to deliver an integrated on-device experience.</p>
         </div>
      </div>
      <div class="inner-ver">
      <div class="col-2 right-text"><h2>Vernacular</h2><br/>
        <p>Create hyper-personalized actionable advertising that speaks to consumers in their own languages to deliver an integrated on-device experience.</p>
         </div>
        <div class="col-2"><img src="{{asset('/images/Verticalization.png')}}"></div>
       
      </div>
   </section>
</div>

<div class="wrapper">
   <section class="home-highImpact">
 
         <h2 class="highImpact-title ">High-Impact Technology at Scale
         </h2>
         <p class=" sub-title-highImpact">Affle's first patent on "Consumer Acceptable Advertising" was filed in 2005 in the U.S. Leading the path of innovation, Affle's technology and its platforms continue to pass the stringent of industry accreditations and uphold the highest standards in ethical advertising. </p>

         <div class="inner pad-0">
           <div class="col-3">
             <img src="{{asset('/images/icon_1.png')}}">
           <h6 class="number">0</h6>
           <p>Connected Devices Reached</p>
           <p class="small"> Period Oct 01, 2019 to Sept 30 2020</p>
           </div>
           <div class="col-3"> 
           <img src="{{asset('/images/icon_2.png')}}">
              <h6 class="number">0</h6>
           <p >Global Clients</p>
           <p class="small">Period Oct 01, 2019 to Sept 30 2020</p></div>
           <div class="col-3">  
           <img src="{{asset('/images/icon_1.png')}}">
              <h6 class="number">0</h6>
           <p>Data Points Processed</p>
           <p class="small">Period Oct 01, 2019 to Sept 30 2020</p></div>
         </div>
         
     
      
   </section>
</div>
<div class="wrapper">
   <section class="home-awards">
 
         <h2 class="awards-title ">Industry Awards
         </h2>

         <div class="inner pad-0">
           <div class="col-4">
           <div class="award-image">
             <img src="{{asset('/images/logo1.png')}}"></div>
           <h6 class="number">India Digital Awards</h6>
           <p>7 Awards in various categories</p>
           
           </div>
           <div class="col-4"> 
           <div class="award-image">
           <img src="{{asset('/images/logo3.png')}}"> </div>
              <h6 class="number">The Maddies</h6>
           <p >9 Awards in various categories</p>
         
         </div>
           <div class="col-4">  
              <div class="award-image">
           <img src="{{asset('/images/logo4.png')}}"> 
       </div>
              <h6 class="number">MMA Smarties</h6>
           <p>9 Awards in various categories</p>
          </div>
          <div class="col-4">  
          <div class="award-image">
        
           <img src="{{asset('/images/logo2.png')}}"></div>
              <h6 class="number">Indian Digital Media Awards</h6>
           <p>4 Awards in various categories</p>
          </div>
         </div>
      
   </section>
   <section class="home-subscribe">
      <div class="form-content">
         <h2>Subscribe to our Newsletter</h2>
         <p>To find out more about us and our platforms and business</p>
         <div class="subs-form">
            <form>
               <input type="email" value="" Placeholder="Email address">
               <button type="submit"> Get Started</button> 
            </form>
            
        </div>
      </div>
   </section> 

</div>