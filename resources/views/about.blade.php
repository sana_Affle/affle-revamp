@extends('layout.app')

@section('content')

<main>
    <section class="about-section1 innerpage-padding">
        <div class="about-section1-inner">
            <h1 class="title">Driven by Passion, Innovation and <br>Entrepreneurial Commitment</h1>
            <div class="stats-box">
                <div class="stats-item">
                    <span class="numbers">12+</span>
                    <span class="text">Countries</span>
                </div>
                <div class="stats-item">
                    <span class="numbers">480+</span>
                    <span class="text">Poeples</span>
                </div>
                <div class="stats-item">
                    <span class="numbers">10+</span>
                    <span class="text">Cultures</span>
                </div>
            </div>
        </div>
        <div class="image-box">
            <img src="{{asset('images/about-image.jpg')}}" alt="" class="img-fluid">
        </div>
    </section>

    <section class="what-we-do">
        <div class="what-we-do-inner">
            <h2 class="title">What We Do</h2>
            <p>Affle is a global technology company with a proprietary consumer intelligence platform that transforms ads into recommendations to drive ROI, delivering consumer engagement, acquisitions, and transactions. While Affle's Consumer platform is used by online and offline companies for measurable mobile advertising, its Enterprise platform helps offline companies to go online through platform-based app development, enablement of O2O commerce, and through its customer data platform.</p>
            <p>Affle India successfully completed its IPO in India on 08.08.19 and now trades on the stock exchanges (BSE: 542752 & NSE: AFFLE). Affle Holdings is the Singapore-based promoter for Affle India and its investors include Microsoft, D2C (an NTT DoCoMo subsidiary), Itochu, Bennett Coleman & Company (BCCL) amongst others.</p>
        </div>
    </section>
    
    <section class="product-milestone">
        <div class="product-milestone-inner">
            <h3 class="title text-center">Corporate & Product Milestones</h3>
            <div class="milestone-box text-center">
                <img src="{{asset('images/milestone.png')}}" alt="" class="img-fluid">
            </div>

            <div class="show-full">
                <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 25 30"><g transform="translate(899.852 -56.148)"><g transform="translate(-152 4)"><g class="a" transform="translate(-744 55.5)"><ellipse class="c" cx="9" cy="9.5" rx="9" ry="9.5"/><ellipse class="d" cx="9" cy="9.5" rx="8.5" ry="9"/></g><path class="b" d="M28.2,15.352A12.852,12.852,0,1,0,15.352,28.2,12.863,12.863,0,0,0,28.2,15.352ZM9.048,17l2-2,2.895,2.895v-9.9h2.814v9.9L19.653,15l2,2-6.3,6.277Z" transform="translate(-750.352 49.648)"/></g></g></svg>
                <span>View All Milestones</span>
            </div>
            
            <div class="show-less">
                <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 25 30"><g transform="translate(899.852 -56.148)"><g transform="translate(-152 4)"><g class="a" transform="translate(-744 55.5)"><ellipse class="c" cx="9" cy="9.5" rx="9" ry="9.5"/><ellipse class="d" cx="9" cy="9.5" rx="8.5" ry="9"/></g><path class="b" d="M28.2,15.352A12.852,12.852,0,1,0,15.352,28.2,12.863,12.863,0,0,0,28.2,15.352ZM9.048,17l2-2,2.895,2.895v-9.9h2.814v9.9L19.653,15l2,2-6.3,6.277Z" transform="translate(-750.352 49.648)"/></g></g></svg>
                <span>View Less</span>
            </div>
        </div>
    </section>

    <section class="our-business-section">
        <h3 class="title">Our Businesses</h3>
        <div class="our-business-section-inner">
            <div class="advertisers-box">
                <div class="text-section">
                    <h4 class="title">For Advertisers</h4>
                </div>
                <div class="image-section">
                    <ul class="logo-list">
                        <li class="logo-item">
                            <img src="{{asset('images/revx-logo.png')}}" alt="RevX" class="img-fluid">
                        </li>
                        <li class="logo-item">
                            <img src="{{asset('images/mediasmart-logo.svg')}}" alt="Mediasmart" class="img-fluid">
                        </li>
                        <li class="logo-item">
                            <img src="{{asset('images/appnext-logo.svg')}}" alt="Appnext" class="img-fluid">
                        </li>
                        <li class="logo-item">
                            <img src="{{asset('images/vizure-logo.png')}}" alt="Vizury" class="img-fluid">
                        </li>
                        <li class="logo-item">
                            <img src="{{asset('images/maas.png')}}" alt="Maas" class="img-fluid">
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="our-business-section-inner">
            <div class="advertisers-box">
                <div class="text-section">
                    <h4 class="title">For Enterprises</h4>
                </div>
                <div class="image-section">
                    <ul class="logo-list">
                        <li class="logo-item">
                            <img src="{{asset('images/mtractionenterprise-logo.png')}}" alt="m Traction Enterprise" class="img-fluid">
                        </li>
                        <li class="logo-item">
                            <img src="{{asset('images/faas-logo.png')}}" alt="FaaS" class="img-fluid">
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section class="management-section">
        <div class="management-section-inner">
            <h3 class="title text-center">Management Team</h3>
            <span class="title-text">Affle India</span>
            <div class="members-list">
                <div class="members-list-item">
                    <div class="members-list-item-inner">
                        <img src="{{asset('images/Anuj_khana.png')}}" alt="Anuj Khanna Sohum" class="img-fluid">
                        <span class="name">Anuj Khanna Sohum</span>
                        <span class="position">Founder, Chairman & Chief Executive Officer</span>
                        <button class="read" type="button" data-bs-toggle="modal" data-bs-target="#mb-popup1"><span>Read Bio</span><svg xmlns="http://www.w3.org/2000/svg" width="30" height="15.663" viewBox="0 0 30 15.663"><path d="M28.463,44.668l-1.537-1.558-5.2,5.2V22.5H19.537V48.311l-5.2-5.2L12.8,44.668,20.632,52.5Z" transform="translate(-22.5 28.463) rotate(-90)"/></svg></button>
                    </div>
                    <div class="modal fade" id="mb-popup1" tabindex="-1" aria-labelledby="mb-popup1" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-lg">
                            <div class="modal-content">
                                <div class="modal-content-inner">
                                    <div class="image-box">
                                        <div class="image">
                                            <img src="{{asset('images/Anuj_khana.png')}}" alt="Anuj Khanna Sohum" class="img-fluid">
                                            <span class="name">Anuj Khanna Sohum</span>
                                            <span class="position">Founder, Chairman & Chief Executive Officer</span>
                                            <a href="#" class="social-url">
                                                <i class="fab fa-linkedin"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <p>Anuj has played a pivotal role in building & growing Affle. He has helped forge important partnerships, integral to Affle’s business, thus helping to make Affle a leading Mobile platform company. Under his leadership, the team has worked with several top marketers globally to help deliver end-to-end mobile advertising and data analytics-driven solutions through Affle’s products and platforms. Prior to Affle, Anuj worked with large global media companies like ESPN STAR Sports, GroupM Mindshare and JWT.</p>
                                        <p>Anuj has over 17 years of relevant media and advertising industry experience and holds a Bachelor’s degree in Economics from St. Stephen's College and a Masters in Advertising & Communication from MICA in India.</p>
                                    </div>
                                </div>
                                <button type="button" class="close-btn" data-bs-dismiss="modal"><i class="fas fa-times"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="members-list-item">
                    <div class="members-list-item-inner">
                        <img src="{{asset('images/anuj_kumar.png')}}" alt="Anuj Kumar" class="img-fluid">
                        <span class="name">Anuj Kumar</span>
                        <span class="position">Co-Founder, Chief Revenue & Operating Officer</span>
                        <button class="read" type="button" data-bs-toggle="modal" data-bs-target="#mb-popup2"><span>Read Bio</span><svg xmlns="http://www.w3.org/2000/svg" width="30" height="15.663" viewBox="0 0 30 15.663"><path d="M28.463,44.668l-1.537-1.558-5.2,5.2V22.5H19.537V48.311l-5.2-5.2L12.8,44.668,20.632,52.5Z" transform="translate(-22.5 28.463) rotate(-90)"/></svg></button>
                    </div>
                    <div class="modal fade" id="mb-popup2" tabindex="-1" aria-labelledby="mb-popup2" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-lg">
                            <div class="modal-content">
                                <div class="modal-content-inner">
                                    <div class="image-box">
                                        <div class="image">
                                            <img src="{{asset('images/anuj_kumar.png')}}" alt="Anuj Khanna Sohum" class="img-fluid">
                                            <span class="name">Anuj Kumar</span>
                                            <span class="position">Co-Founder, Chief Revenue & Operating Officer</span>
                                            <a href="#" class="social-url">
                                                <i class="fab fa-linkedin"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <p>Anuj has played a pivotal role in building & growing Affle. He has helped forge important partnerships, integral to Affle’s business, thus helping to make Affle a leading Mobile platform company. Under his leadership, the team has worked with several top marketers globally to help deliver end-to-end mobile advertising and data analytics-driven solutions through Affle’s products and platforms. Prior to Affle, Anuj worked with large global media companies like ESPN STAR Sports, GroupM Mindshare and JWT.</p>
                                        <p>Anuj has over 17 years of relevant media and advertising industry experience and holds a Bachelor’s degree in Economics from St. Stephen's College and a Masters in Advertising & Communication from MICA in India.</p>
                                    </div>
                                </div>
                                <button type="button" class="close-btn" data-bs-dismiss="modal"><i class="fas fa-times"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="members-list-item">
                    <div class="members-list-item-inner">
                        <img src="{{asset('images/kapil_bhutani.png')}}" alt="Kapil Bhutani" class="img-fluid">
                        <span class="name">Kapil Bhutani</span>
                        <span class="position">Chief Financial & Operations Officer</span>
                        <button class="read" type="button" data-bs-toggle="modal" data-bs-target="#mb-popup3"><span>Read Bio</span><svg xmlns="http://www.w3.org/2000/svg" width="30" height="15.663" viewBox="0 0 30 15.663"><path d="M28.463,44.668l-1.537-1.558-5.2,5.2V22.5H19.537V48.311l-5.2-5.2L12.8,44.668,20.632,52.5Z" transform="translate(-22.5 28.463) rotate(-90)"/></svg></button>
                    </div>
                    <div class="modal fade" id="mb-popup3" tabindex="-1" aria-labelledby="mb-popup3" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-lg">
                            <div class="modal-content">
                                <div class="modal-content-inner">
                                    <div class="image-box">
                                        <div class="image">
                                            <img src="{{asset('images/kapil_bhutani.png')}}" alt="Kapil Bhutani" class="img-fluid">
                                            <span class="name">Kapil Bhutani</span>
                                            <span class="position">Chief Financial & Operations Officer</span>
                                            <a href="#" class="social-url">
                                                <i class="fab fa-linkedin"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <p>Anuj has played a pivotal role in building & growing Affle. He has helped forge important partnerships, integral to Affle’s business, thus helping to make Affle a leading Mobile platform company. Under his leadership, the team has worked with several top marketers globally to help deliver end-to-end mobile advertising and data analytics-driven solutions through Affle’s products and platforms. Prior to Affle, Anuj worked with large global media companies like ESPN STAR Sports, GroupM Mindshare and JWT.</p>
                                        <p>Anuj has over 17 years of relevant media and advertising industry experience and holds a Bachelor’s degree in Economics from St. Stephen's College and a Masters in Advertising & Communication from MICA in India.</p>
                                    </div>
                                </div>
                                <button type="button" class="close-btn" data-bs-dismiss="modal"><i class="fas fa-times"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="members-list-item">
                    <div class="members-list-item-inner">
                        <img src="{{asset('images/vipul_kedia.png')}}" alt="Vipul Kedia" class="img-fluid">
                        <span class="name">Vipul Kedia</span>
                        <span class="position">Chief Data & Platforms Officer (Maas)</span>                        
                        <button class="read" type="button" data-bs-toggle="modal" data-bs-target="#mb-popup4"><span>Read Bio</span><svg xmlns="http://www.w3.org/2000/svg" width="30" height="15.663" viewBox="0 0 30 15.663"><path d="M28.463,44.668l-1.537-1.558-5.2,5.2V22.5H19.537V48.311l-5.2-5.2L12.8,44.668,20.632,52.5Z" transform="translate(-22.5 28.463) rotate(-90)"/></svg></button>
                    </div>
                    <div class="modal fade" id="mb-popup4" tabindex="-1" aria-labelledby="mb-popup4" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-lg">
                            <div class="modal-content">
                                <div class="modal-content-inner">
                                    <div class="image-box">
                                        <div class="image">
                                            <img src="{{asset('images/vipul_kedia.png')}}" alt="Anuj Khanna Sohum" class="img-fluid">
                                            <span class="name">Vipul Kedia</span>
                                            <span class="position">Chief Data & Platforms Officer (Maas)</span>
                                            <a href="#" class="social-url">
                                                <i class="fab fa-linkedin"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <p>Anuj has played a pivotal role in building & growing Affle. He has helped forge important partnerships, integral to Affle’s business, thus helping to make Affle a leading Mobile platform company. Under his leadership, the team has worked with several top marketers globally to help deliver end-to-end mobile advertising and data analytics-driven solutions through Affle’s products and platforms. Prior to Affle, Anuj worked with large global media companies like ESPN STAR Sports, GroupM Mindshare and JWT.</p>
                                        <p>Anuj has over 17 years of relevant media and advertising industry experience and holds a Bachelor’s degree in Economics from St. Stephen's College and a Masters in Advertising & Communication from MICA in India.</p>
                                    </div>
                                </div>
                                <button type="button" class="close-btn" data-bs-dismiss="modal"><i class="fas fa-times"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="members-list-item">
                    <div class="members-list-item-inner">
                        <img src="{{asset('images/charles_yong.png')}}" class="img-fluid" alt="Affle's Investors">
                        <span class="name">Charles Yong</span>
                        <span class="position">Founder, Chairman & Chief Executive Officer</span>                        
                        <button class="read" type="button" data-bs-toggle="modal" data-bs-target="#mb-popup5"><span>Read Bio</span><svg xmlns="http://www.w3.org/2000/svg" width="30" height="15.663" viewBox="0 0 30 15.663"><path d="M28.463,44.668l-1.537-1.558-5.2,5.2V22.5H19.537V48.311l-5.2-5.2L12.8,44.668,20.632,52.5Z" transform="translate(-22.5 28.463) rotate(-90)"/></svg></button>
                    </div>
                    <div class="modal fade" id="mb-popup5" tabindex="-1" aria-labelledby="mb-popup5" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-lg">
                            <div class="modal-content">
                                <div class="modal-content-inner">
                                    <div class="image-box">
                                        <div class="image">
                                            <img src="{{asset('images/charles_yong.png')}}" class="img-fluid" alt="Affle's Investors">
                                            <span class="name">Charles Yong</span>
                                            <span class="position">Founder, Chairman & Chief Executive Officer</span>
                                            <a href="#" class="social-url">
                                                <i class="fab fa-linkedin"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <p>Anuj has played a pivotal role in building & growing Affle. He has helped forge important partnerships, integral to Affle’s business, thus helping to make Affle a leading Mobile platform company. Under his leadership, the team has worked with several top marketers globally to help deliver end-to-end mobile advertising and data analytics-driven solutions through Affle’s products and platforms. Prior to Affle, Anuj worked with large global media companies like ESPN STAR Sports, GroupM Mindshare and JWT.</p>
                                        <p>Anuj has over 17 years of relevant media and advertising industry experience and holds a Bachelor’s degree in Economics from St. Stephen's College and a Masters in Advertising & Communication from MICA in India.</p>
                                    </div>
                                </div>
                                <button type="button" class="close-btn" data-bs-dismiss="modal"><i class="fas fa-times"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="members-list-item">
                    <div class="members-list-item-inner">
                        <img src="{{asset('images/madan.png')}}" alt="Madan Sanglikar" class="img-fluid">
                        <span class="name">Madan Sanglikar</span>
                        <span class="position">Co-Founder, Managing Partner - South East Asia & Chief Of Staff</span>                        
                        <button class="read" type="button" data-bs-toggle="modal" data-bs-target="#mb-popup6"><span>Read Bio</span><svg xmlns="http://www.w3.org/2000/svg" width="30" height="15.663" viewBox="0 0 30 15.663"><path d="M28.463,44.668l-1.537-1.558-5.2,5.2V22.5H19.537V48.311l-5.2-5.2L12.8,44.668,20.632,52.5Z" transform="translate(-22.5 28.463) rotate(-90)"/></svg></button>
                    </div>
                    <div class="modal fade" id="mb-popup6" tabindex="-1" aria-labelledby="mb-popup6" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-lg">
                            <div class="modal-content">
                                <div class="modal-content-inner">
                                    <div class="image-box">
                                        <div class="image">
                                            <img src="{{asset('images/madan.png')}}" alt="Madan Sanglikar" class="img-fluid">
                                            <span class="name">Madan Sanglikar</span>
                                            <span class="position">Co-Founder, Managing Partner - South East Asia & Chief Of Staff</span>
                                            <a href="#" class="social-url">
                                                <i class="fab fa-linkedin"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <p>Anuj has played a pivotal role in building & growing Affle. He has helped forge important partnerships, integral to Affle’s business, thus helping to make Affle a leading Mobile platform company. Under his leadership, the team has worked with several top marketers globally to help deliver end-to-end mobile advertising and data analytics-driven solutions through Affle’s products and platforms. Prior to Affle, Anuj worked with large global media companies like ESPN STAR Sports, GroupM Mindshare and JWT.</p>
                                        <p>Anuj has over 17 years of relevant media and advertising industry experience and holds a Bachelor’s degree in Economics from St. Stephen's College and a Masters in Advertising & Communication from MICA in India.</p>
                                    </div>
                                </div>
                                <button type="button" class="close-btn" data-bs-dismiss="modal"><i class="fas fa-times"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="members-list-item">
                    <div class="members-list-item-inner">
                        <img src="{{asset('images/viraj.png')}}" alt="Viraj Sinh" class="img-fluid">
                        <span class="name">Viraj Sinh</span>
                        <span class="position">Co-Founder, Managing Partner - International (Maas)</span>                        
                        <button class="read" type="button" data-bs-toggle="modal" data-bs-target="#mb-popup7"><span>Read Bio</span><svg xmlns="http://www.w3.org/2000/svg" width="30" height="15.663" viewBox="0 0 30 15.663"><path d="M28.463,44.668l-1.537-1.558-5.2,5.2V22.5H19.537V48.311l-5.2-5.2L12.8,44.668,20.632,52.5Z" transform="translate(-22.5 28.463) rotate(-90)"/></svg></button>
                    </div>
                    <div class="modal fade" id="mb-popup7" tabindex="-1" aria-labelledby="mb-popup7" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-lg">
                            <div class="modal-content">
                                <div class="modal-content-inner">
                                    <div class="image-box">
                                        <div class="image">
                                            <img src="{{asset('images/viraj.png')}}" alt="Viraj Sinh" class="img-fluid">
                                            <span class="name">Viraj Sinh</span>
                                            <span class="position">Co-Founder, Managing Partner - International (Maas)</span>
                                            <a href="#" class="social-url">
                                                <i class="fab fa-linkedin"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <p>Anuj has played a pivotal role in building & growing Affle. He has helped forge important partnerships, integral to Affle’s business, thus helping to make Affle a leading Mobile platform company. Under his leadership, the team has worked with several top marketers globally to help deliver end-to-end mobile advertising and data analytics-driven solutions through Affle’s products and platforms. Prior to Affle, Anuj worked with large global media companies like ESPN STAR Sports, GroupM Mindshare and JWT.</p>
                                        <p>Anuj has over 17 years of relevant media and advertising industry experience and holds a Bachelor’s degree in Economics from St. Stephen's College and a Masters in Advertising & Communication from MICA in India.</p>
                                    </div>
                                </div>
                                <button type="button" class="close-btn" data-bs-dismiss="modal"><i class="fas fa-times"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="members-list-item">
                    <div class="members-list-item-inner">
                        <img src="{{asset('images/elad_dp.png')}}" alt="Elad Natanson" class="img-fluid">
                        <span class="name">Elad Natanson</span>
                        <span class="position">Chief Executive Officer (Appnext)</span>                        
                        <button class="read" type="button" data-bs-toggle="modal" data-bs-target="#mb-popup8"><span>Read Bio</span><svg xmlns="http://www.w3.org/2000/svg" width="30" height="15.663" viewBox="0 0 30 15.663"><path d="M28.463,44.668l-1.537-1.558-5.2,5.2V22.5H19.537V48.311l-5.2-5.2L12.8,44.668,20.632,52.5Z" transform="translate(-22.5 28.463) rotate(-90)"/></svg></button>
                    </div>
                    <div class="modal fade" id="mb-popup8" tabindex="-1" aria-labelledby="mb-popup8" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-lg">
                            <div class="modal-content">
                                <div class="modal-content-inner">
                                    <div class="image-box">
                                        <div class="image">
                                            <img src="{{asset('images/elad_dp.png')}}" alt="Elad Natanson" class="img-fluid">
                                            <span class="name">Elad Natanson</span>
                                            <span class="position">Chief Executive Officer (Appnext)</span>
                                            <a href="#" class="social-url">
                                                <i class="fab fa-linkedin"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <p>Anuj has played a pivotal role in building & growing Affle. He has helped forge important partnerships, integral to Affle’s business, thus helping to make Affle a leading Mobile platform company. Under his leadership, the team has worked with several top marketers globally to help deliver end-to-end mobile advertising and data analytics-driven solutions through Affle’s products and platforms. Prior to Affle, Anuj worked with large global media companies like ESPN STAR Sports, GroupM Mindshare and JWT.</p>
                                        <p>Anuj has over 17 years of relevant media and advertising industry experience and holds a Bachelor’s degree in Economics from St. Stephen's College and a Masters in Advertising & Communication from MICA in India.</p>
                                    </div>
                                </div>
                                <button type="button" class="close-btn" data-bs-dismiss="modal"><i class="fas fa-times"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="members-list-item">
                    <div class="members-list-item-inner">
                        <img src="{{asset('images/noelia.png')}}" alt="Noelia Amoedo" class="img-fluid">
                        <span class="name">Noelia Amoedo</span>
                        <span class="position">Chief Executive Officer (Mediasmart)</span>                        
                        <button class="read" type="button" data-bs-toggle="modal" data-bs-target="#mb-popup9"><span>Read Bio</span><svg xmlns="http://www.w3.org/2000/svg" width="30" height="15.663" viewBox="0 0 30 15.663"><path d="M28.463,44.668l-1.537-1.558-5.2,5.2V22.5H19.537V48.311l-5.2-5.2L12.8,44.668,20.632,52.5Z" transform="translate(-22.5 28.463) rotate(-90)"/></svg></button>
                    </div>
                    <div class="modal fade" id="mb-popup9" tabindex="-1" aria-labelledby="mb-popup9" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-lg">
                            <div class="modal-content">
                                <div class="modal-content-inner">
                                    <div class="image-box">
                                        <div class="image">
                                            <img src="{{asset('images/noelia.png')}}" alt="Noelia Amoedo" class="img-fluid">
                                            <span class="name">Noelia Amoedo</span>
                                            <span class="position">Chief Executive Officer (Mediasmart)</span>
                                            <a href="#" class="social-url">
                                                <i class="fab fa-linkedin"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <p>Anuj has played a pivotal role in building & growing Affle. He has helped forge important partnerships, integral to Affle’s business, thus helping to make Affle a leading Mobile platform company. Under his leadership, the team has worked with several top marketers globally to help deliver end-to-end mobile advertising and data analytics-driven solutions through Affle’s products and platforms. Prior to Affle, Anuj worked with large global media companies like ESPN STAR Sports, GroupM Mindshare and JWT.</p>
                                        <p>Anuj has over 17 years of relevant media and advertising industry experience and holds a Bachelor’s degree in Economics from St. Stephen's College and a Masters in Advertising & Communication from MICA in India.</p>
                                    </div>
                                </div>
                                <button type="button" class="close-btn" data-bs-dismiss="modal"><i class="fas fa-times"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="members-list-item">
                    <div class="members-list-item-inner">
                        <img src="{{asset('images/martje_dp.png')}}" alt="Martje Abeldt" class="img-fluid">
                        <span class="name">Martje Abeldt</span>
                        <span class="position">Chief Revenue Officer (Revx)</span>                        
                        <button class="read" type="button" data-bs-toggle="modal" data-bs-target="#mb-popup10"><span>Read Bio</span><svg xmlns="http://www.w3.org/2000/svg" width="30" height="15.663" viewBox="0 0 30 15.663"><path d="M28.463,44.668l-1.537-1.558-5.2,5.2V22.5H19.537V48.311l-5.2-5.2L12.8,44.668,20.632,52.5Z" transform="translate(-22.5 28.463) rotate(-90)"/></svg></button>
                    </div>
                    <div class="modal fade" id="mb-popup10" tabindex="-1" aria-labelledby="mb-popup10" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-lg">
                            <div class="modal-content">
                                <div class="modal-content-inner">
                                    <div class="image-box">
                                        <div class="image">
                                            <img src="{{asset('images/martje_dp.png')}}" alt="Martje Abeldt" class="img-fluid">
                                            <span class="name">Martje Abeldt</span>
                                            <span class="position">Chief Revenue Officer (Revx)</span>
                                            <a href="#" class="social-url">
                                                <i class="fab fa-linkedin"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <p>Anuj has played a pivotal role in building & growing Affle. He has helped forge important partnerships, integral to Affle’s business, thus helping to make Affle a leading Mobile platform company. Under his leadership, the team has worked with several top marketers globally to help deliver end-to-end mobile advertising and data analytics-driven solutions through Affle’s products and platforms. Prior to Affle, Anuj worked with large global media companies like ESPN STAR Sports, GroupM Mindshare and JWT.</p>
                                        <p>Anuj has over 17 years of relevant media and advertising industry experience and holds a Bachelor’s degree in Economics from St. Stephen's College and a Masters in Advertising & Communication from MICA in India.</p>
                                    </div>
                                </div>
                                <button type="button" class="close-btn" data-bs-dismiss="modal"><i class="fas fa-times"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="members-list-item">
                    <div class="members-list-item-inner">
                        <img src="{{asset('images/dp_sujoy.png')}}" alt="Sujoy Golan" class="img-fluid">
                        <span class="name">Sujoy Golan</span>
                        <span class="position">Chief Of Marketing & Omnichannel Platforms</span>                        
                        <button class="read" type="button" data-bs-toggle="modal" data-bs-target="#mb-popup11"><span>Read Bio</span><svg xmlns="http://www.w3.org/2000/svg" width="30" height="15.663" viewBox="0 0 30 15.663"><path d="M28.463,44.668l-1.537-1.558-5.2,5.2V22.5H19.537V48.311l-5.2-5.2L12.8,44.668,20.632,52.5Z" transform="translate(-22.5 28.463) rotate(-90)"/></svg></button>
                    </div>
                    <div class="modal fade" id="mb-popup11" tabindex="-1" aria-labelledby="mb-popup11" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-lg">
                            <div class="modal-content">
                                <div class="modal-content-inner">
                                    <div class="image-box">
                                        <div class="image">
                                            <img src="{{asset('images/dp_sujoy.png')}}" alt="Sujoy Golan" class="img-fluid">
                                            <span class="name">Sujoy Golan</span>
                                            <span class="position">Chief Of Marketing & Omnichannel Platforms</span>
                                            <a href="#" class="social-url">
                                                <i class="fab fa-linkedin"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <p>Anuj has played a pivotal role in building & growing Affle. He has helped forge important partnerships, integral to Affle’s business, thus helping to make Affle a leading Mobile platform company. Under his leadership, the team has worked with several top marketers globally to help deliver end-to-end mobile advertising and data analytics-driven solutions through Affle’s products and platforms. Prior to Affle, Anuj worked with large global media companies like ESPN STAR Sports, GroupM Mindshare and JWT.</p>
                                        <p>Anuj has over 17 years of relevant media and advertising industry experience and holds a Bachelor’s degree in Economics from St. Stephen's College and a Masters in Advertising & Communication from MICA in India.</p>
                                    </div>
                                </div>
                                <button type="button" class="close-btn" data-bs-dismiss="modal"><i class="fas fa-times"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="members-list-item">
                    <div class="members-list-item-inner">
                        <img src="{{asset('images/danny_dp.png')}}" alt="Danny Tuttnauer" class="img-fluid">
                        <span class="name">Danny Tuttnauer</span>
                        <span class="position">Chief Operating Officer (Appnext)</span>                        
                        <button class="read" type="button" data-bs-toggle="modal" data-bs-target="#mb-popup12"><span>Read Bio</span><svg xmlns="http://www.w3.org/2000/svg" width="30" height="15.663" viewBox="0 0 30 15.663"><path d="M28.463,44.668l-1.537-1.558-5.2,5.2V22.5H19.537V48.311l-5.2-5.2L12.8,44.668,20.632,52.5Z" transform="translate(-22.5 28.463) rotate(-90)"/></svg></button>
                    </div>
                    <div class="modal fade" id="mb-popup12" tabindex="-1" aria-labelledby="mb-popup12" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-lg">
                            <div class="modal-content">
                                <div class="modal-content-inner">
                                    <div class="image-box">
                                        <div class="image">
                                            <img src="{{asset('images/danny_dp.png')}}" alt="Danny Tuttnauer" class="img-fluid">
                                            <span class="name">Danny Tuttnauer</span>
                                            <span class="position">Chief Operating Officer (Appnext)</span>
                                            <a href="#" class="social-url">
                                                <i class="fab fa-linkedin"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <p>Anuj has played a pivotal role in building & growing Affle. He has helped forge important partnerships, integral to Affle’s business, thus helping to make Affle a leading Mobile platform company. Under his leadership, the team has worked with several top marketers globally to help deliver end-to-end mobile advertising and data analytics-driven solutions through Affle’s products and platforms. Prior to Affle, Anuj worked with large global media companies like ESPN STAR Sports, GroupM Mindshare and JWT.</p>
                                        <p>Anuj has over 17 years of relevant media and advertising industry experience and holds a Bachelor’s degree in Economics from St. Stephen's College and a Masters in Advertising & Communication from MICA in India.</p>
                                    </div>
                                </div>
                                <button type="button" class="close-btn" data-bs-dismiss="modal"><i class="fas fa-times"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="members-list-item">
                    <div class="members-list-item-inner">
                        <img src="{{asset('images/ankit.png')}}" alt="Ankit Rawal" class="img-fluid">
                        <span class="name">Ankit Rawal</span>
                        <span class="position">Director - Business Development & Partnerships (Maas)</span>                        
                        <button class="read" type="button" data-bs-toggle="modal" data-bs-target="#mb-popup13"><span>Read Bio</span><svg xmlns="http://www.w3.org/2000/svg" width="30" height="15.663" viewBox="0 0 30 15.663"><path d="M28.463,44.668l-1.537-1.558-5.2,5.2V22.5H19.537V48.311l-5.2-5.2L12.8,44.668,20.632,52.5Z" transform="translate(-22.5 28.463) rotate(-90)"/></svg></button>
                    </div>
                    <div class="modal fade" id="mb-popup13" tabindex="-1" aria-labelledby="mb-popup13" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-lg">
                            <div class="modal-content">
                                <div class="modal-content-inner">
                                    <div class="image-box">
                                        <div class="image">
                                            <img src="{{asset('images/ankit.png')}}" alt="Ankit Rawal" class="img-fluid">
                                            <span class="name">Ankit Rawal</span>
                                            <span class="position">Director - Business Development & Partnerships (Maas)</span>
                                            <a href="#" class="social-url">
                                                <i class="fab fa-linkedin"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <p>Anuj has played a pivotal role in building & growing Affle. He has helped forge important partnerships, integral to Affle’s business, thus helping to make Affle a leading Mobile platform company. Under his leadership, the team has worked with several top marketers globally to help deliver end-to-end mobile advertising and data analytics-driven solutions through Affle’s products and platforms. Prior to Affle, Anuj worked with large global media companies like ESPN STAR Sports, GroupM Mindshare and JWT.</p>
                                        <p>Anuj has over 17 years of relevant media and advertising industry experience and holds a Bachelor’s degree in Economics from St. Stephen's College and a Masters in Advertising & Communication from MICA in India.</p>
                                    </div>
                                </div>
                                <button type="button" class="close-btn" data-bs-dismiss="modal"><i class="fas fa-times"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="members-list-item">
                    <div class="members-list-item-inner">
                        <img src="{{asset('images/eran_dp.png')}}" alt="Eran Kariti" class="img-fluid">
                        <span class="name">Eran Kariti</span>
                        <span class="position">Chief Technology Officer (Appnext)</span>                        
                        <button class="read" type="button" data-bs-toggle="modal" data-bs-target="#mb-popup14"><span>Read Bio</span><svg xmlns="http://www.w3.org/2000/svg" width="30" height="15.663" viewBox="0 0 30 15.663"><path d="M28.463,44.668l-1.537-1.558-5.2,5.2V22.5H19.537V48.311l-5.2-5.2L12.8,44.668,20.632,52.5Z" transform="translate(-22.5 28.463) rotate(-90)"/></svg></button>
                    </div>
                    <div class="modal fade" id="mb-popup14" tabindex="-1" aria-labelledby="mb-popup14" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-lg">
                            <div class="modal-content">
                                <div class="modal-content-inner">
                                    <div class="image-box">
                                        <div class="image">
                                            <img src="{{asset('images/eran_dp.png')}}" alt="Eran Kariti" class="img-fluid">
                                            <span class="name">Eran Kariti</span>
                                            <span class="position">Chief Technology Officer (Appnext)</span>
                                            <a href="#" class="social-url">
                                                <i class="fab fa-linkedin"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <p>Anuj has played a pivotal role in building & growing Affle. He has helped forge important partnerships, integral to Affle’s business, thus helping to make Affle a leading Mobile platform company. Under his leadership, the team has worked with several top marketers globally to help deliver end-to-end mobile advertising and data analytics-driven solutions through Affle’s products and platforms. Prior to Affle, Anuj worked with large global media companies like ESPN STAR Sports, GroupM Mindshare and JWT.</p>
                                        <p>Anuj has over 17 years of relevant media and advertising industry experience and holds a Bachelor’s degree in Economics from St. Stephen's College and a Masters in Advertising & Communication from MICA in India.</p>
                                    </div>
                                </div>
                                <button type="button" class="close-btn" data-bs-dismiss="modal"><i class="fas fa-times"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="members-list-item">
                    <div class="members-list-item-inner">
                        <img src="{{asset('images/guillermo.png')}}" alt="Guillermo Fernandez Sanz" class="img-fluid">
                        <span class="name">Guillermo Fernandez Sanz</span>
                        <span class="position">Chief Technology Officer (Mediasmart)</span>                        
                        <button class="read" type="button" data-bs-toggle="modal" data-bs-target="#mb-popup15"><span>Read Bio</span><svg xmlns="http://www.w3.org/2000/svg" width="30" height="15.663" viewBox="0 0 30 15.663"><path d="M28.463,44.668l-1.537-1.558-5.2,5.2V22.5H19.537V48.311l-5.2-5.2L12.8,44.668,20.632,52.5Z" transform="translate(-22.5 28.463) rotate(-90)"/></svg></button>
                    </div>
                    <div class="modal fade" id="mb-popup15" tabindex="-1" aria-labelledby="mb-popup15" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-lg">
                            <div class="modal-content">
                                <div class="modal-content-inner">
                                    <div class="image-box">
                                        <div class="image">
                                            <img src="{{asset('images/guillermo.png')}}" alt="Guillermo Fernandez Sanz" class="img-fluid">
                                            <span class="name">Guillermo Fernandez Sanz</span>
                                            <span class="position">Chief Technology Officer (Mediasmart)</span>
                                            <a href="#" class="social-url">
                                                <i class="fab fa-linkedin"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <p>Anuj has played a pivotal role in building & growing Affle. He has helped forge important partnerships, integral to Affle’s business, thus helping to make Affle a leading Mobile platform company. Under his leadership, the team has worked with several top marketers globally to help deliver end-to-end mobile advertising and data analytics-driven solutions through Affle’s products and platforms. Prior to Affle, Anuj worked with large global media companies like ESPN STAR Sports, GroupM Mindshare and JWT.</p>
                                        <p>Anuj has over 17 years of relevant media and advertising industry experience and holds a Bachelor’s degree in Economics from St. Stephen's College and a Masters in Advertising & Communication from MICA in India.</p>
                                    </div>
                                </div>
                                <button type="button" class="close-btn" data-bs-dismiss="modal"><i class="fas fa-times"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="members-list-item">
                    <div class="members-list-item-inner">
                        <img src="{{asset('images/nag.png')}}" alt="Nagendra Dhanakeerthi" class="img-fluid">
                        <span class="name">Nagendra Dhanakeerthi</span>
                        <span class="position">Chief Technology Officer (Vizury)</span>                        
                        <button class="read" type="button" data-bs-toggle="modal" data-bs-target="#mb-popup16"><span>Read Bio</span><svg xmlns="http://www.w3.org/2000/svg" width="30" height="15.663" viewBox="0 0 30 15.663"><path d="M28.463,44.668l-1.537-1.558-5.2,5.2V22.5H19.537V48.311l-5.2-5.2L12.8,44.668,20.632,52.5Z" transform="translate(-22.5 28.463) rotate(-90)"/></svg></button>
                    </div>
                    <div class="modal fade" id="mb-popup16" tabindex="-1" aria-labelledby="mb-popup16" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-lg">
                            <div class="modal-content">
                                <div class="modal-content-inner">
                                    <div class="image-box">
                                        <div class="image">
                                            <img src="{{asset('images/nag.png')}}" alt="Nagendra Dhanakeerthi" class="img-fluid">
                                            <span class="name">Nagendra Dhanakeerthi</span>
                                            <span class="position">Chief Technology Officer (Vizury)</span>
                                            <a href="#" class="social-url">
                                                <i class="fab fa-linkedin"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <p>Anuj has played a pivotal role in building & growing Affle. He has helped forge important partnerships, integral to Affle’s business, thus helping to make Affle a leading Mobile platform company. Under his leadership, the team has worked with several top marketers globally to help deliver end-to-end mobile advertising and data analytics-driven solutions through Affle’s products and platforms. Prior to Affle, Anuj worked with large global media companies like ESPN STAR Sports, GroupM Mindshare and JWT.</p>
                                        <p>Anuj has over 17 years of relevant media and advertising industry experience and holds a Bachelor’s degree in Economics from St. Stephen's College and a Masters in Advertising & Communication from MICA in India.</p>
                                    </div>
                                </div>
                                <button type="button" class="close-btn" data-bs-dismiss="modal"><i class="fas fa-times"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="members-list-item">
                    <div class="members-list-item-inner">
                        <img src="{{asset('images/kp.png')}}" alt="Kulpreet Singh" class="img-fluid">
                        <span class="name">Kulpreet Singh</span>
                        <span class="position">Lead Architect & Technology Director (Maas)</span>                        
                        <button class="read" type="button" data-bs-toggle="modal" data-bs-target="#mb-popup17"><span>Read Bio</span><svg xmlns="http://www.w3.org/2000/svg" width="30" height="15.663" viewBox="0 0 30 15.663"><path d="M28.463,44.668l-1.537-1.558-5.2,5.2V22.5H19.537V48.311l-5.2-5.2L12.8,44.668,20.632,52.5Z" transform="translate(-22.5 28.463) rotate(-90)"/></svg></button>
                    </div>
                    <div class="modal fade" id="mb-popup17" tabindex="-1" aria-labelledby="mb-popup17" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-lg">
                            <div class="modal-content">
                                <div class="modal-content-inner">
                                    <div class="image-box">
                                        <div class="image">
                                            <img src="{{asset('images/kp.png')}}" alt="Kulpreet Singh" class="img-fluid">
                                            <span class="name">Kulpreet Singh</span>
                                            <span class="position">Lead Architect & Technology Director (Maas)</span>    
                                            <a href="#" class="social-url">
                                                <i class="fab fa-linkedin"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <p>Anuj has played a pivotal role in building & growing Affle. He has helped forge important partnerships, integral to Affle’s business, thus helping to make Affle a leading Mobile platform company. Under his leadership, the team has worked with several top marketers globally to help deliver end-to-end mobile advertising and data analytics-driven solutions through Affle’s products and platforms. Prior to Affle, Anuj worked with large global media companies like ESPN STAR Sports, GroupM Mindshare and JWT.</p>
                                        <p>Anuj has over 17 years of relevant media and advertising industry experience and holds a Bachelor’s degree in Economics from St. Stephen's College and a Masters in Advertising & Communication from MICA in India.</p>
                                    </div>
                                </div>
                                <button type="button" class="close-btn" data-bs-dismiss="modal"><i class="fas fa-times"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <span class="title-text">Affle Holdings</span>
            <div class="members-list">
                <div class="members-list-item">
                    <div class="members-list-item-inner">
                        <img src="{{asset('images/madhu.png')}}" alt="Madhusudana Ramakrishna" class="img-fluid">
                        <span class="name">Madhusudana Ramakrishna</span>
                        <span class="position">Co-Founder, Director Of Innovation & Corporate Development</span>
                        <button class="read" type="button" data-bs-toggle="modal" data-bs-target="#mb-popup18"><span>Read Bio</span><svg xmlns="http://www.w3.org/2000/svg" width="30" height="15.663" viewBox="0 0 30 15.663"><path d="M28.463,44.668l-1.537-1.558-5.2,5.2V22.5H19.537V48.311l-5.2-5.2L12.8,44.668,20.632,52.5Z" transform="translate(-22.5 28.463) rotate(-90)"/></svg></button>
                    </div>
                    <div class="modal fade" id="mb-popup18" tabindex="-1" aria-labelledby="mb-popup18" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-lg">
                            <div class="modal-content">
                                <div class="modal-content-inner">
                                    <div class="image-box">
                                        <div class="image">
                                            <img src="{{asset('images/madhu.png')}}" alt="Madhusudana Ramakrishna" class="img-fluid">
                                            <span class="name">Madhusudana Ramakrishna</span>
                                            <span class="position">Co-Founder, Director Of Innovation & Corporate Development</span>
                                            <a href="#" class="social-url">
                                                <i class="fab fa-linkedin"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <p>Anuj has played a pivotal role in building & growing Affle. He has helped forge important partnerships, integral to Affle’s business, thus helping to make Affle a leading Mobile platform company. Under his leadership, the team has worked with several top marketers globally to help deliver end-to-end mobile advertising and data analytics-driven solutions through Affle’s products and platforms. Prior to Affle, Anuj worked with large global media companies like ESPN STAR Sports, GroupM Mindshare and JWT.</p>
                                        <p>Anuj has over 17 years of relevant media and advertising industry experience and holds a Bachelor’s degree in Economics from St. Stephen's College and a Masters in Advertising & Communication from MICA in India.</p>
                                    </div>
                                </div>
                                <button type="button" class="close-btn" data-bs-dismiss="modal"><i class="fas fa-times"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="members-list-item">
                    <div class="members-list-item-inner">
                        <img src="{{asset('images/mei_theng.png')}}" alt="Mei Theng Leong" class="img-fluid">
                        <span class="name">Mei Theng Leong</span>
                        <span class="position">Chief Financial & Commercial Officer – International</span>
                        <button class="read" type="button" data-bs-toggle="modal" data-bs-target="#mb-popup19"><span>Read Bio</span><svg xmlns="http://www.w3.org/2000/svg" width="30" height="15.663" viewBox="0 0 30 15.663"><path d="M28.463,44.668l-1.537-1.558-5.2,5.2V22.5H19.537V48.311l-5.2-5.2L12.8,44.668,20.632,52.5Z" transform="translate(-22.5 28.463) rotate(-90)"/></svg></button>
                    </div>
                    <div class="modal fade" id="mb-popup19" tabindex="-1" aria-labelledby="mb-popup19" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-lg">
                            <div class="modal-content">
                                <div class="modal-content-inner">
                                    <div class="image-box">
                                        <div class="image">
                                            <img src="{{asset('images/mei_theng.png')}}" alt="Mei Theng Leong" class="img-fluid">
                                            <span class="name">Mei Theng Leong</span>
                                            <span class="position">Chief Financial & Commercial Officer – International</span>
                                            <a href="#" class="social-url">
                                                <i class="fab fa-linkedin"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <p>Anuj has played a pivotal role in building & growing Affle. He has helped forge important partnerships, integral to Affle’s business, thus helping to make Affle a leading Mobile platform company. Under his leadership, the team has worked with several top marketers globally to help deliver end-to-end mobile advertising and data analytics-driven solutions through Affle’s products and platforms. Prior to Affle, Anuj worked with large global media companies like ESPN STAR Sports, GroupM Mindshare and JWT.</p>
                                        <p>Anuj has over 17 years of relevant media and advertising industry experience and holds a Bachelor’s degree in Economics from St. Stephen's College and a Masters in Advertising & Communication from MICA in India.</p>
                                    </div>
                                </div>
                                <button type="button" class="close-btn" data-bs-dismiss="modal"><i class="fas fa-times"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="management-section-inner">
            <h3 class="title text-center">Non Executive Directors</h3>
            <span class="title-text">Affle India</span>
            <div class="members-list">
                <div class="members-list-item">
                    <div class="members-list-item-inner">
                        <img src="{{asset('images/Vivek_Gour.png')}}" alt="Vivek N Gour" class="img-fluid">
                        <span class="name">Vivek N Gour</span>
                        <span class="position">Independent Non Executive Director</span>
                        <button class="read" type="button" data-bs-toggle="modal" data-bs-target="#mb-popup20"><span>Read Bio</span><svg xmlns="http://www.w3.org/2000/svg" width="30" height="15.663" viewBox="0 0 30 15.663"><path d="M28.463,44.668l-1.537-1.558-5.2,5.2V22.5H19.537V48.311l-5.2-5.2L12.8,44.668,20.632,52.5Z" transform="translate(-22.5 28.463) rotate(-90)"/></svg></button>
                    </div>
                    <div class="modal fade" id="mb-popup20" tabindex="-1" aria-labelledby="mb-popup20" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-lg">
                            <div class="modal-content">
                                <div class="modal-content-inner">
                                    <div class="image-box">
                                        <div class="image">
                                            <img src="{{asset('images/Vivek_Gour.png')}}" alt="Vivek N Gour" class="img-fluid">
                                            <span class="name">Vivek N Gour</span>
                                            <span class="position">Independent Non Executive Director</span>
                                            <a href="#" class="social-url">
                                                <i class="fab fa-linkedin"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <p>Anuj has played a pivotal role in building & growing Affle. He has helped forge important partnerships, integral to Affle’s business, thus helping to make Affle a leading Mobile platform company. Under his leadership, the team has worked with several top marketers globally to help deliver end-to-end mobile advertising and data analytics-driven solutions through Affle’s products and platforms. Prior to Affle, Anuj worked with large global media companies like ESPN STAR Sports, GroupM Mindshare and JWT.</p>
                                        <p>Anuj has over 17 years of relevant media and advertising industry experience and holds a Bachelor’s degree in Economics from St. Stephen's College and a Masters in Advertising & Communication from MICA in India.</p>
                                    </div>
                                </div>
                                <button type="button" class="close-btn" data-bs-dismiss="modal"><i class="fas fa-times"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="members-list-item">
                    <div class="members-list-item-inner">
                        <img src="{{asset('images/nawal.png')}}" alt="Bijay Nawal" class="img-fluid">
                        <span class="name">Bijay Nawal</span>
                        <span class="position">Independent Non Executive Director</span>
                        <button class="read" type="button" data-bs-toggle="modal" data-bs-target="#mb-popup21"><span>Read Bio</span><svg xmlns="http://www.w3.org/2000/svg" width="30" height="15.663" viewBox="0 0 30 15.663"><path d="M28.463,44.668l-1.537-1.558-5.2,5.2V22.5H19.537V48.311l-5.2-5.2L12.8,44.668,20.632,52.5Z" transform="translate(-22.5 28.463) rotate(-90)"/></svg></button>
                    </div>
                    <div class="modal fade" id="mb-popup21" tabindex="-1" aria-labelledby="mb-popup21" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-lg">
                            <div class="modal-content">
                                <div class="modal-content-inner">
                                    <div class="image-box">
                                        <div class="image">
                                            <img src="{{asset('images/nawal.png')}}" alt="Bijay Nawal" class="img-fluid">
                                            <span class="name">Bijay Nawal</span>
                                            <span class="position">Independent Non Executive Director</span>
                                            <a href="#" class="social-url">
                                                <i class="fab fa-linkedin"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <p>Anuj has played a pivotal role in building & growing Affle. He has helped forge important partnerships, integral to Affle’s business, thus helping to make Affle a leading Mobile platform company. Under his leadership, the team has worked with several top marketers globally to help deliver end-to-end mobile advertising and data analytics-driven solutions through Affle’s products and platforms. Prior to Affle, Anuj worked with large global media companies like ESPN STAR Sports, GroupM Mindshare and JWT.</p>
                                        <p>Anuj has over 17 years of relevant media and advertising industry experience and holds a Bachelor’s degree in Economics from St. Stephen's College and a Masters in Advertising & Communication from MICA in India.</p>
                                    </div>
                                </div>
                                <button type="button" class="close-btn" data-bs-dismiss="modal"><i class="fas fa-times"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="members-list-item">
                    <div class="members-list-item-inner">
                        <img src="{{asset('images/SUMITM_CHADHA.png')}}" alt="Sumit Chadha" class="img-fluid">
                        <span class="name">Sumit Chadha</span>
                        <span class="position">Independent Non Executive Director</span>
                        <button class="read" type="button" data-bs-toggle="modal" data-bs-target="#mb-popup22"><span>Read Bio</span><svg xmlns="http://www.w3.org/2000/svg" width="30" height="15.663" viewBox="0 0 30 15.663"><path d="M28.463,44.668l-1.537-1.558-5.2,5.2V22.5H19.537V48.311l-5.2-5.2L12.8,44.668,20.632,52.5Z" transform="translate(-22.5 28.463) rotate(-90)"/></svg></button>
                    </div>
                    <div class="modal fade" id="mb-popup22" tabindex="-1" aria-labelledby="mb-popup22" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-lg">
                            <div class="modal-content">
                                <div class="modal-content-inner">
                                    <div class="image-box">
                                        <div class="image">
                                            <img src="{{asset('images/SUMITM_CHADHA.png')}}" alt="Sumit Chadha" class="img-fluid">
                                            <span class="name">Sumit Chadha</span>
                                            <span class="position">Independent Non Executive Director</span>
                                            <a href="#" class="social-url">
                                                <i class="fab fa-linkedin"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <p>Anuj has played a pivotal role in building & growing Affle. He has helped forge important partnerships, integral to Affle’s business, thus helping to make Affle a leading Mobile platform company. Under his leadership, the team has worked with several top marketers globally to help deliver end-to-end mobile advertising and data analytics-driven solutions through Affle’s products and platforms. Prior to Affle, Anuj worked with large global media companies like ESPN STAR Sports, GroupM Mindshare and JWT.</p>
                                        <p>Anuj has over 17 years of relevant media and advertising industry experience and holds a Bachelor’s degree in Economics from St. Stephen's College and a Masters in Advertising & Communication from MICA in India.</p>
                                    </div>
                                </div>
                                <button type="button" class="close-btn" data-bs-dismiss="modal"><i class="fas fa-times"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <span class="title-text">Affle Holdings</span>
            <div class="members-list">
                <div class="members-list-item">
                    <div class="members-list-item-inner">
                        <img src="{{asset('images/richard_humphreys.png')}}" alt="Richard Humphreys" class="img-fluid">
                        <span class="name">Richard Humphreys</span>
                        <span class="position">Non Executive Director</span>
                        <button class="read" type="button" data-bs-toggle="modal" data-bs-target="#mb-popup23"><span>Read Bio</span><svg xmlns="http://www.w3.org/2000/svg" width="30" height="15.663" viewBox="0 0 30 15.663"><path d="M28.463,44.668l-1.537-1.558-5.2,5.2V22.5H19.537V48.311l-5.2-5.2L12.8,44.668,20.632,52.5Z" transform="translate(-22.5 28.463) rotate(-90)"/></svg></button>
                    </div>
                    <div class="modal fade" id="mb-popup23" tabindex="-1" aria-labelledby="mb-popup23" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-lg">
                            <div class="modal-content">
                                <div class="modal-content-inner">
                                    <div class="image-box">
                                        <div class="image">
                                            <img src="{{asset('images/richard_humphreys.png')}}" alt="Richard Humphreys" class="img-fluid">
                                            <span class="name">Richard Humphreys</span>
                                            <span class="position">Non Executive Director</span>
                                            <a href="#" class="social-url">
                                                <i class="fab fa-linkedin"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <p>Anuj has played a pivotal role in building & growing Affle. He has helped forge important partnerships, integral to Affle’s business, thus helping to make Affle a leading Mobile platform company. Under his leadership, the team has worked with several top marketers globally to help deliver end-to-end mobile advertising and data analytics-driven solutions through Affle’s products and platforms. Prior to Affle, Anuj worked with large global media companies like ESPN STAR Sports, GroupM Mindshare and JWT.</p>
                                        <p>Anuj has over 17 years of relevant media and advertising industry experience and holds a Bachelor’s degree in Economics from St. Stephen's College and a Masters in Advertising & Communication from MICA in India.</p>
                                    </div>
                                </div>
                                <button type="button" class="close-btn" data-bs-dismiss="modal"><i class="fas fa-times"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="members-list-item">
                    <div class="members-list-item-inner">
                        <img src="{{asset('images/takayuki.png')}}" alt="Takayuki Hoshuyama" class="img-fluid">
                        <span class="name">Takayuki Hoshuyama</span>
                        <span class="position">Honorary Advisor</span>
                        <button class="read" type="button" data-bs-toggle="modal" data-bs-target="#mb-popup24"><span>Read Bio</span><svg xmlns="http://www.w3.org/2000/svg" width="30" height="15.663" viewBox="0 0 30 15.663"><path d="M28.463,44.668l-1.537-1.558-5.2,5.2V22.5H19.537V48.311l-5.2-5.2L12.8,44.668,20.632,52.5Z" transform="translate(-22.5 28.463) rotate(-90)"/></svg></button>
                    </div>
                    <div class="modal fade" id="mb-popup24" tabindex="-1" aria-labelledby="mb-popup24" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-lg">
                            <div class="modal-content">
                                <div class="modal-content-inner">
                                    <div class="image-box">
                                        <div class="image">
                                            <img src="{{asset('images/takayuki.png')}}" alt="Takayuki Hoshuyama" class="img-fluid">
                                            <span class="name">Takayuki Hoshuyama</span>
                                            <span class="position">Honorary Advisor</span>
                                            <a href="#" class="social-url">
                                                <i class="fab fa-linkedin"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <p>Anuj has played a pivotal role in building & growing Affle. He has helped forge important partnerships, integral to Affle’s business, thus helping to make Affle a leading Mobile platform company. Under his leadership, the team has worked with several top marketers globally to help deliver end-to-end mobile advertising and data analytics-driven solutions through Affle’s products and platforms. Prior to Affle, Anuj worked with large global media companies like ESPN STAR Sports, GroupM Mindshare and JWT.</p>
                                        <p>Anuj has over 17 years of relevant media and advertising industry experience and holds a Bachelor’s degree in Economics from St. Stephen's College and a Masters in Advertising & Communication from MICA in India.</p>
                                    </div>
                                </div>
                                <button type="button" class="close-btn" data-bs-dismiss="modal"><i class="fas fa-times"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="members-list-item">
                    <div class="members-list-item-inner">
                        <img src="{{asset('images/jay.png')}}" alt="Jay Snynder" class="img-fluid">
                        <span class="name">Jay Snynder</span>
                        <span class="position">Independent Observer</span>
                        <button class="read" type="button" data-bs-toggle="modal" data-bs-target="#mb-popup25"><span>Read Bio</span><svg xmlns="http://www.w3.org/2000/svg" width="30" height="15.663" viewBox="0 0 30 15.663"><path d="M28.463,44.668l-1.537-1.558-5.2,5.2V22.5H19.537V48.311l-5.2-5.2L12.8,44.668,20.632,52.5Z" transform="translate(-22.5 28.463) rotate(-90)"/></svg></button>
                    </div>
                    <div class="modal fade" id="mb-popup25" tabindex="-1" aria-labelledby="mb-popup25" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-lg">
                            <div class="modal-content">
                                <div class="modal-content-inner">
                                    <div class="image-box">
                                        <div class="image">
                                            <img src="{{asset('images/jay.png')}}" alt="Jay Snynder" class="img-fluid">
                                            <span class="name">Jay Snynder</span>
                                            <span class="position">Independent Observer</span>
                                            <a href="#" class="social-url">
                                                <i class="fab fa-linkedin"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <p>Anuj has played a pivotal role in building & growing Affle. He has helped forge important partnerships, integral to Affle’s business, thus helping to make Affle a leading Mobile platform company. Under his leadership, the team has worked with several top marketers globally to help deliver end-to-end mobile advertising and data analytics-driven solutions through Affle’s products and platforms. Prior to Affle, Anuj worked with large global media companies like ESPN STAR Sports, GroupM Mindshare and JWT.</p>
                                        <p>Anuj has over 17 years of relevant media and advertising industry experience and holds a Bachelor’s degree in Economics from St. Stephen's College and a Masters in Advertising & Communication from MICA in India.</p>
                                    </div>
                                </div>
                                <button type="button" class="close-btn" data-bs-dismiss="modal"><i class="fas fa-times"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="members-list-item">
                    <div class="members-list-item-inner">
                        <img src="{{asset('images/nawal.png')}}" alt="Bijay Nawal" class="img-fluid">
                        <span class="name">Bijay Nawal</span>
                        <span class="position">Independent Non Executive Director</span>
                        <button class="read" type="button" data-bs-toggle="modal" data-bs-target="#mb-popup26"><span>Read Bio</span><svg xmlns="http://www.w3.org/2000/svg" width="30" height="15.663" viewBox="0 0 30 15.663"><path d="M28.463,44.668l-1.537-1.558-5.2,5.2V22.5H19.537V48.311l-5.2-5.2L12.8,44.668,20.632,52.5Z" transform="translate(-22.5 28.463) rotate(-90)"/></svg></button>
                    </div>
                    <div class="modal fade" id="mb-popup26" tabindex="-1" aria-labelledby="mb-popup26" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-lg">
                            <div class="modal-content">
                                <div class="modal-content-inner">
                                    <div class="image-box">
                                        <div class="image">
                                            <img src="{{asset('images/nawal.png')}}" alt="Bijay Nawal" class="img-fluid">
                                            <span class="name">Bijay Nawal</span>
                                            <span class="position">Independent Non Executive Director</span>
                                            <a href="#" class="social-url">
                                                <i class="fab fa-linkedin"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <p>Anuj has played a pivotal role in building & growing Affle. He has helped forge important partnerships, integral to Affle’s business, thus helping to make Affle a leading Mobile platform company. Under his leadership, the team has worked with several top marketers globally to help deliver end-to-end mobile advertising and data analytics-driven solutions through Affle’s products and platforms. Prior to Affle, Anuj worked with large global media companies like ESPN STAR Sports, GroupM Mindshare and JWT.</p>
                                        <p>Anuj has over 17 years of relevant media and advertising industry experience and holds a Bachelor’s degree in Economics from St. Stephen's College and a Masters in Advertising & Communication from MICA in India.</p>
                                    </div>
                                </div>
                                <button type="button" class="close-btn" data-bs-dismiss="modal"><i class="fas fa-times"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="investor-section">
        <div class="investor-section-inner">
            <h3 class="title text-center">Our Investors</h3>
            <p class="title-text">Affle Holdings ("AHPL")</p>
            <div class="investor-images text-center">
                <img src="{{asset('images/investors.png')}}" alt="" class="img-fluid">
            </div>
        </div>
    </section>
</main>


@endsection