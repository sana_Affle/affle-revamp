@extends('layout.app')

@section('content')
<main>
    <section class="banner-section">
        <div class="banner-section-inner">
            <div class="content-box">
                <h1 class="title">Creating Impactful Consumer Journeys on Connected Devices</h1>
                <p>Build real customer connections throughtout the user's journeys to drive user acquisition, engagement, and conversions.</p>
                <a href="#" class="btn btn--primary">Learn More <img src="{{asset('images/arrow.svg')}}" alt="" class="img-fluid"></a>
            </div>
        </div>
    </section>

    <section class="ad-solution-section">
        <div class="ad-solution-section-inner">
            <div class="title-section text-center">
                <h2 class="title">Transform Ads Into Recommendations With Data-Driven Ad Solutions</h2>
                <p>Discover and identify your most valuable users, and then continue to acquire and re-engage them through Affle's platforms and offerings. Designed to help marketers connect with consumers, our deep-learning, AI-powered algorithms transform ads into powerful consumer recommendations to deliver enhanced ROI and engagement.</p>
            </div>
            <div class="box-section">
                <div class="box box-1">
                    <div class="box-inner">
                        <h3 class="title">Discover & Identify</h3>
                        <ul class="list-style">
                            <li class="list-item">Audience Data Intellienge <img src="{{asset('images/arrow-long.svg')}}" alt="" class="img-fluid"></li>
                            <li class="list-item">App Discovery <img src="{{asset('images/arrow-long.svg')}}" alt="" class="img-fluid"></li>
                            <li class="list-item">Ad Fraud Prevention <img src="{{asset('images/arrow-long.svg')}}" alt="" class="img-fluid"></li>
                            <li class="list-item">Proximity Marketing & Connected TV <img src="{{asset('images/arrow-long.svg')}}" alt="" class="img-fluid"></li>
                        </ul>
                    </div>
                </div>
                <div class="box box-2">
                    <div class="box-inner">
                        <h3 class="title">Acquire & Re-engage</h3>
                        <ul class="list-style">
                            <li class="list-item">Programmatic Acquisition & Retention <img src="{{asset('images/arrow-long.svg')}}" alt="" class="img-fluid"></li>
                            <li class="list-item">App Recommendations <img src="{{asset('images/arrow-long.svg')}}" alt="" class="img-fluid"></li>
                            <li class="list-item">Omnichannel & CRM <img src="{{asset('images/arrow-long.svg')}}" alt="" class="img-fluid"></li>
                            <li class="list-item">Unified Audience Platform <img src="{{asset('images/arrow-long.svg')}}" alt="" class="img-fluid"></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="growth-section">
        <div class="growth-section-inner">
            <div class="title-section col-xl-8">
                <h3 class="title">Vernacular and Verticalization: Driving Our Growth Story</h3>
                <p>Our growth story is anchored in the 2Vs - vernacular and verticalization. Aiming to reach >10Bn connected devices including connected TV, personal wearables, and smart household appliances, we create unique and integrated consumer journeys for marketers to get more out of their advertising.</p>
            </div>
            <div class="content-box">
                <div class="row content-box-inner">
                    <div class="col-md-6 content-box-image">
                        <img src="{{asset('images/vernacular-image.png')}}" alt="Vernacular" class="img-fluid">
                    </div>
                    <div class="col-md-6 content-box-details">
                        <h3 class="title">Vernacular</h3>
                        <p>Create hyper-personalized actionable advertising that speaks to consumers in their own languages to deliver an integrated on-device experience.</p>
                    </div>
                </div>
                <div class="row content-box-inner">
                    <div class="col-md-6 content-box-image">
                        <img src="{{asset('images/verticalization-image.png')}}" alt="Vernacular" class="img-fluid">
                    </div>
                    <div class="col-md-6 content-box-details">
                        <h3 class="title">Verticalization</h3>
                        <p>Achieve higher conversions with advertising that is tailor-made for your vertical and drive exceptional performance through deeper competitive insights.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="stats-section">
        <div class="stats-section-inner">
            <div class="title-section text-center">
                <h3 class="title">High-Impact Technology at Scale</h3>
                <p>Affle's first patent on "Consumer Acceptable Advertising" was filed in 2005 in the U.S. Leading the path of innovation, Affle's technology and its platforms continue to pass the stringent of industry accreditations and uphold the highest standards in ethical advertising.</p>
            </div>
            <div class="row stats-box-section">
                <div class="stats-box col-lg-4">
                    <div class="stats-box-inner">
                        <img src="{{asset('images/icon_1.png')}}" alt="" class="img-fluid">
                        <h3 class="title">2,192,200,398</h3>
                        <p class="text">Connected Devices Reached</p>
                        <p class="date">Period Oct 01, 2019 to Sept 30 2020</p>
                    </div>
                </div>
                <div class="stats-box col-lg-4">
                    <div class="stats-box-inner">
                        <img src="{{asset('images/icon_2.png')}}" alt="" class="img-fluid">
                        <h3 class="title">3900</h3>
                        <p class="text">Global Clients</p>
                        <p class="date">Period Oct 01, 2019 to Sept 30 2020</p>
                    </div>
                </div>
                <div class="stats-box col-lg-4">
                    <div class="stats-box-inner">
                        <img src="{{asset('images/icon_1.png')}}" alt="" class="img-fluid">
                        <h3 class="title">979,436,652,477</h3>
                        <p class="text">Period Oct 01, 2019 to Sept 30 2020</p>
                        <p class="date">Period Oct 01, 2019 to Sept 30 2020</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('inc.awards')
</main>
@endsection