@extends('layout.app')

@section('content')

<main>
    <div class="investor-menu">
        <a href="#">
            <svg xmlns="http://www.w3.org/2000/svg" width="19.779" height="13.6" viewBox="0 0 19.779 13.6"><defs><style>.a{fill:#ffffff;}</style></defs><g transform="translate(0 123)"><g transform="translate(0 64)"><g transform="translate(0 -187)"><rect class="a" width="19.779" height="1.6"/></g><g transform="translate(0 -181)"><rect class="a" width="19.779" height="1.6"/></g><g transform="translate(0 -175)"><rect class="a" width="19.779" height="1.6"/></g></g></g></svg>
            <span>Investor</span><span> Menu</span>
        </a>
    </div>


    <section class="corporate-governance innerpage-padding shareholders-section">
        <div class="corporate-governance-inner">
            <div class="title-section">
                <h1 class="title"><span>Shareholders Information</span></h1>
            </div>
            
            <div class="accordion" id="corporate-accordion">
                <div class="accordion-item">
                    <h2 class="accordion-header" id="cp-title">
                        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#cp-content" aria-expanded="true" aria-controls="cp-content">Placement Document</button>
                    </h2>
                    <div id="cp-content" class="accordion-collapse collapse show" aria-labelledby="cp-title" data-bs-parent="#corporate-accordion">
                        <div class="accordion-body">
                            <ul>
                                <li><a href="#">Placement Document May 04, 2021</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="cbc-title">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#cbc-content" aria-expanded="false" aria-controls="cbc-content">Special Purpose Interim Condensed Financial Statements</button>
                    </h2>
                    <div id="cbc-content" class="accordion-collapse collapse" aria-labelledby="cbc-title" data-bs-parent="#corporate-accordion">
                        <div class="accordion-body">
                            <ul>
                                <li><a href="#">Special Purpose Interim Condensed Consolidated Financial Statements As at and For the Nine Months Ended December 31, 2020</a></li>
                                <li><a href="#">Special Purpose Interim Condensed Consolidated Financial Statements As at and For the Nine Months Ended December 31, 2019</a></li>
                                <li><a href="#">Special Purpose Interim Condensed Standalone Financial Statements As at and For the Nine Months Ended December 31, 2020</a></li>
                                <li><a href="#">Special Purpose Interim Condensed Standalone Financial Statements As at and For the Nine Months Ended December 31, 2019</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>


@endsection