<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Affle</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link rel="stylesheet" href="{{ url('/') }}/css/manrope.css" />
    <link rel="stylesheet" href="{{ url('/') }}/css/poppins.css" />
    <link rel="stylesheet" href="{{ url('/') }}/css/style.css" />
	<link rel="icon" href="/images/favicon.ico" type="image/png" sizes="16x16">
</head>
<body>
   @include('inc.header')

 <?php //include('inc.side_menu') ?>

  @yield('content')
  @include('inc.newsletter')
  @include('inc.footer')

  <div class="quick-btn">
    <a href="#">
        <img src="{{asset('images/quick-btn.svg')}}" alt="" class="img-fluid">
    </a>
</div>

<script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
<script type="text/javascript" src="{{asset('js/custom.js')}}"></script>
</body>
</html>