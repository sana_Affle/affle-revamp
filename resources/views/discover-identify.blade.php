@extends('layout.app')

@section('content')
<main>
    <section class="banner-section discover-identify-banner">
        <div class="banner-section-inner">
            <div class="content-box">
                <h1 class="title">Discover & Identify</h1>
                <p>Whether it is to organize and enrich campaigns with your own data or to boost your monetization stack, Affle's mDMP data management technology allows you to easily customize and build segments over raw data.</p>
                <div class="banner-bottom">
                    <h4 class="subtitle">Discover Our Offerings</h4>
                    <a href="#" class="btn"><img src="{{asset('images/arrow-blue.svg')}}" alt="" class="img-fluid"></a>
                </div>
            </div>
        </div>
    </section>

    <section class="image-text-section">
        <div class="image-text-section-inner">
            <div class="box box1">
                <div class="box-inner">
                    <div class="head-icon"><img src="{{asset('images/dmp.png')}}" alt="" class="img-fluid"></div>
                    <h4 class="title">Lorem ipsum dolor amet, consetetur sadipscing elitr sedar diam nonumy</h4>
                    <ul class="list">
                        <li class="list-item">
                            <span class="icon"><img src="{{asset('images/users.svg')}}" alt="" class="img-fluid"></span>
                            <span class="text">Lorem ipsum dolor sit amet, consetetur elitr sadipscing elitr sed diam nonumy</span>
                        </li>
                        <li class="list-item">
                            <span class="icon"><img src="{{asset('images/graph.svg')}}" alt="" class="img-fluid"></span>
                            <span class="text">Lorem ipsum dolor sit amet, consetetur elitr sadipscing elitr sed diam nonumy</span>
                        </li>
                        <li class="list-item">
                            <span class="icon"><img src="{{asset('images/grid.svg')}}" alt="" class="img-fluid"></span>
                            <span class="text">Lorem ipsum dolor sit amet, consetetur elitr sadipscing elitr sed diam nonumy</span>
                        </li>
                    </ul>
                    <a href="#" class="btn btn--primary">EXPLORE mDMP</a>
                </div>
            </div>
            <div class="box box2">
                <div class="box-inner"></div>
            </div>
        </div>
    </section>

    @include('inc.awards')
</main>
@endsection