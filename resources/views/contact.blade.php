@extends('layout.app')

@section('content')
<main>

    <section class="contact-section1 innerpage-padding">
        <div class="contact-section1-inner">
            <div class="form-section">
                <h1 class="title">Get In Touch</h1>
                <form class="contact-form">
                    <div class="contact-form-inner">
                        <div class="form-group">
                            <label class="label-text">Name</label>
                            <input type="text" class="form-control form-input" name="name">
                        </div>
                        <div class="form-group">
                            <label class="label-text">Email<sup>*</sup></label>
                            <input type="email" class="form-control form-input" name="email" required>
                        </div>
                        <div class="form-group">
                            <label class="label-text">Organization Name<sup>*</sup></label>
                            <input type="text" class="form-control form-input" name="name" required>
                        </div>
                        <div class="form-group">
                            <label>Intrested In<sup>*</sup></label>
                            <select class="txt-field" name="interest" id="interest">
                                <option value="-None-" disabled="disabled" selected="selected">Interested In*</option>
                                <option value="Mobile Advertising - User Acquisition">Mobile Advertising - User Acquisition</option>
                                <option value="Mobile Advertising - User Acquisition">Mobile Advertising - Proximity Marketing</option>
                                <option value="Mobile Advertising - User Acquisition">Mobile Advertising - Omnichannel Retargeting</option>
                                <option value="Chat Bots">Chat Bots</option>
                                <option value="Data Management">Data Management</option>
                                <option value="Audience Intelligence">Audience Intelligence</option>
                                <option value="Fraud Detection">Fraud Detection</option>
                                <option value="Ad Monetization">Ad Monetization</option>
                                <option value="Rich Media &amp; Video">Rich Media &amp; Video</option>
                                <option value="App Developmen">App Development</option>
                                <option value="O2O Commerce">O2O Commerce</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="label-text">Mobile Number</label>
                            <input type="text" class="form-control form-input" name="mobile">
                        </div>
                        <div class="form-group">
                            <label class="label-text">Skype ID</label>
                            <input type="text" class="form-control form-input" name="skype">
                        </div>
                        <div class="form-group textarea-box">
                            <label class="label-text">Tell us a bit about your project</label>
                            <textarea name="description" cols="30" rows="1" class="form-control form-input" maxlength="1000" spellcheck="false"></textarea>
                        </div>
                    </div>
                    <div class="form-button">
                        <input type="submit" class="btn btn--primary" id="addContact" value="Submit">
                        <input type="reset" class="btn btn--primary-outline" value="Reset ">
                    </div>
                </form>
            </div>
            <div class="address-section">
                <div class="address-section-inner">
                    <h3 class="title">For other queries, please write to us at:</h3>
                    <div class="details">
                        <div class="details-inner">
                            <span class="head">Partnerships</span>
                            <a href="mailto:partners@affle.com" class="highlight">partners@affle.com</a>
                        </div>
                        <div class="details-inner">
                            <span class="head">Data & Privacy</span>
                            <a href="mailto:dpo@affle.com" class="highlight">dpo@affle.com</a>
                        </div>
                        <div class="details-inner">
                            <span class="head">Investor</span>
                            <a href="mailto:investor.relations@affle.com" class="highlight">investor.relations@affle.com</a>
                        </div>
                        <div class="details-inner">
                            <span class="head">Media</span>
                            <a href="mailto:pr@affle.com" class="highlight">pr@affle.com</a>
                        </div>
                    </div>
                    <div class="social-networks">
                        <h3 class="title">Follow Us</h3>
                        <a href="#" class="social-item"><i class="fab fa-facebook-f"></i></a>
                        <a href="#" class="social-item"><i class="fab fa-twitter"></i></a>
                        <a href="#" class="social-item"><i class="fab fa-instagram"></i></a>
                        <a href="#" class="social-item"><i class="fab fa-linkedin-in"></i></a>
                        <a href="#" class="social-item"><i class="fab fa-youtube"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="address-location-section">
        <div class="address-location-section-inner">
            <div class="title-section">
                <h3 class="title">Our Offices</h3>
                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod</p>
            </div>
            <div class="address-box">
                <div class="address-box-item">
                    <div class="image">
                        <img src="{{asset('images/gurugram.jpg')}}" alt="Gurugram (India)" class="img-fluid">
                    </div>
                    <h3 class="name">Gurugram (India)</h3>
                    <p class="address">601 - 612, 6th Floor, Tower C, JMD Megapolis, Sector-48, Sohna Road, Gurgaon, Haryana - 122018, India</p>
                    <div class="email">
                        <span class="icon">
                            <img src="{{asset('images/email.png')}}" alt="" class="img-fluid">
                        </span>
                        <a href="mailto:india@affle.com">india@affle.com</a>
                    </div>
                </div>
                <div class="address-box-item">
                    <div class="image">
                        <img src="{{asset('images/bangalore.jpg')}}" alt="Bangalore (India)" class="img-fluid">
                    </div>
                    <h3 class="name">Bangalore (India)</h3>
                    <p class="address">IndiQube Lexington, situated at Municipal, No.18, 2nd Cross Road, Chikka Audugodi, Bengaluru - 560 029, India</p>
                    <div class="email">
                        <span class="icon">
                            <img src="{{asset('images/email.png')}}" alt="" class="img-fluid">
                        </span>
                        <a href="mailto:india@affle.com">india@affle.com</a>
                    </div>
                </div>
                <div class="address-box-item">
                    <div class="image">
                        <img src="{{asset('images/mumbai.jpg')}}" alt="Mumbai (India)" class="img-fluid">
                    </div>
                    <h3 class="name">Mumbai (India)</h3>
                    <p class="address">B-312, Kanakia Wall Street, Hanuman <span>Nagar, Andheri East, Mumbai,</span> <span>Maharashtra 400047</span></p>
                    <div class="email">
                        <span class="icon">
                            <img src="{{asset('images/email.png')}}" alt="" class="img-fluid">
                        </span>
                        <a href="mailto:india@affle.com">india@affle.com</a>
                    </div>
                </div>
                <div class="address-box-item">
                    <div class="image">
                        <img src="{{asset('images/singapore.jpg')}}" alt="Singapore" class="img-fluid">
                    </div>
                    <h3 class="name">Singapore</h3>
                    <p class="address">100 Pasir Panjang Road, #06-07 Singapore, 118518</p>
                    <div class="email">
                        <span class="icon">
                            <img src="{{asset('images/email.png')}}" alt="" class="img-fluid">
                        </span>
                        <a href="mailto:singapore@affle.com">singapore@affle.com</a>
                    </div>
                </div>
                <div class="address-box-item">
                    <div class="image">
                        <img src="{{asset('images/spain.jpg')}}" alt="Spain" class="img-fluid">
                    </div>
                    <h3 class="name">Spain</h3>
                    <p class="address">Calle García de Paredes 12, 1º B, Madrid, <span>28010</span></p>
                    <div class="email">
                        <span class="icon">
                            <img src="{{asset('images/email.png')}}" alt="" class="img-fluid">
                        </span>
                        <a href="mailto:partners@affle.com">partners@affle.com</a>
                    </div>
                </div>
                <div class="address-box-item">
                    <div class="image">
                        <img src="{{asset('images/uae.jpg')}}" alt="UAE" class="img-fluid">
                    </div>
                    <h3 class="name">UAE</h3>
                    <p class="address">Affle MEA, Building 17, Office 351, Third Floor, Dubai Internet City, Dubai, UAE</p>
                    <div class="email">
                        <span class="icon">
                            <img src="{{asset('images/email.png')}}" alt="" class="img-fluid">
                        </span>
                        <a href="mailto:partners@affle.com">partners@affle.com</a>
                    </div>
                </div>
                <div class="address-box-item">
                    <div class="image">
                        <img src="{{asset('images/indonesia.jpg')}}" alt="Indonesia" class="img-fluid">
                    </div>
                    <h3 class="name">Indonesia</h3>
                    <p class="address">PT. Affle Indonesia, WeWork Noble House, <span>Noble House, 30th Floor, Jl Dr. Ide</span> Anak Agung Gde Agung Kav E.4. No. 2, Kuningan, Jakarta Selatan 12950</p>
                    <div class="email">
                        <span class="icon">
                            <img src="{{asset('images/email.png')}}" alt="" class="img-fluid">
                        </span>
                        <a href="mailto:indonesia@affle.com">indonesia@affle.com</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

@endsection