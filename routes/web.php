<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/', 'PagesController@index');
Route::get('/about', 'PagesController@about');
Route::get('/career', 'PagesController@career');
Route::get('/contact', 'PagesController@contact');
Route::get('/corporate-governance', 'PagesController@corporategovernance');
Route::get('/shareholders', 'PagesController@shareholders');
Route::get('/investor-relations', 'PagesController@investorrelations');
Route::get('/corporate-action', 'PagesController@corporateaction');
Route::get('/media', 'PagesController@media');
Route::get('/discover-identify', 'PagesController@discoverIdentify');